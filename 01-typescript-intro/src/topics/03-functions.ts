function addNumbers( a: number, b:number):number {
    return a+b
}


// lambda functions or arrow funtions

const addNumbersArrow = (a: number, b: number):string => {
    const sum = (a + b).toString();
    return sum;
}

function multiply(firstNumber: number, secondNumber?: number, base: number = 2): number {
    return firstNumber * base
};

let result: string = addNumbers(2,2).toString()
let resultArrow: string = addNumbersArrow(5,5)
let resultMultiply: number = multiply(5)


console.log({ result })
console.log({ resultArrow });
console.log({ resultMultiply });

interface Character {
    name: string;
    hp: number;
    showHp: () => void;
}

const healCharacter = (character: Character, amount: number) => {
    character.hp += amount;
}

const strider: Character = {
    name: 'Stider',
    hp: 50,
    showHp() {
        console.log(`Puntos de vida ${ this.hp }`)
    }
}

strider.showHp();

export {}